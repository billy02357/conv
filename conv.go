package conv

import "fmt"

// TEMPERATURE
type Celsius float64
type Fahrenheit float64
type Kelvin float64

// Celsius constants
const (
	AbsoluteZeroC Celsius = -273.15
	FreezingC     Celsius = 0
	BoilingC      Celsius = 100
)

// Fahrenheit constants
const (
	AbsoluteZeroF Fahrenheit = -459.67
	FreezingF     Fahrenheit = 32
	BoilingF      Fahrenheit = 212
)

// Kelvin constants
const (
	AbsoluteZeroK Kelvin = 0
	FreezingK     Kelvin = 273.15
	BoilingK      Kelvin = 373.15
)

// Celsius
func CToF(c Celsius) Fahrenheit {
	return Fahrenheit(c*9/5 + 32)
}

func CToK(c Celsius) Kelvin {
	return Kelvin(c + 273.15)
}

// Fahrenheit
func FToC(f Fahrenheit) Celsius {
	return Celsius((f - 32) * 5 / 9)
}

func FToK(f Fahrenheit) Kelvin {
	return Kelvin((f-32)*5/9 + 273.15)
}

// Kelvin
func KToC(k Kelvin) Celsius {
	return Celsius(k - 273.15)
}

func KToF(k Kelvin) Fahrenheit {
	return Fahrenheit((k-273.15)*9/5 + 32)
}

// Representation
func (c Celsius) String() string {
	return fmt.Sprintf("%gºC", c)
}

func (f Fahrenheit) String() string {
	return fmt.Sprintf("%gºF", f)
}

func (k Kelvin) String() string {
	return fmt.Sprintf("%gºK", k)
}

// WEIGHT
type Pound float64
type Kilogram float64

func PToKG(p Pound) Kilogram {
	return Kilogram(p / 2.2046)
}

func KGToP(kg Kilogram) Pound {
	return Pound(kg * 2.2046)
}

// DISTANCE
type Meter float64
type Centimeter float64
type Millimeter float64
type Mile float64
type Inch float64
type Foot float64

// Meters
func MToMI(m Meter) Mile {
	return Mile(m / 1609.344)
}

func MToIN(m Meter) Inch {
	return Inch(m * 39.370079)
}

func MToCM(m Meter) Centimeter {
	return Centimeter(m * 100)
}

func MToMM(m Meter) Millimeter {
	return Millimeter(m * 1000)
}

func MToFT(m Meter) Foot {
	return Foot(m * 3.2808)
}

// Centimeters
func CMToM(cm Centimeter) Meter {
	return Meter(cm / 100)
}

func CMToMM(cm Centimeter) Millimeter {
	return Millimeter(cm * 10)
}

func CMToMI(cm Centimeter) Mile {
	return Mile(cm * 0.0000062137)
}

func CMToIN(cm Centimeter) Inch {
	return Inch(cm * 0.39370)
}

func CMToFT(cm Centimeter) Foot {
	return Foot(cm * 0.032808)
}

// Millimeters
func MMToM(mm Millimeter) Meter {
	return Meter(mm / 1000)
}

func MMToCM(mm Millimeter) Centimeter {
	return Centimeter(mm / 10)
}

func MMToMI(mm Millimeter) Mile {
	return Mile(mm * 0.00000062137)
}

func MMToIN(mm Millimeter) Inch {
	return Inch(mm * 0.039370)
}

func MMToFT(mm Millimeter) Foot {
	return Foot(mm * 0.0032808)
}

// Miles
func MIToM(mi Mile) Meter {
	return Meter(mi * 1609.344)
}

func MIToIN(mi Mile) Inch {
	return Inch(mi * 63360)
}

func MIToFT(mi Mile) Foot {
	return Foot(mi * 5280.0)
}

func MIToCM(mi Mile) Centimeter {
	return Centimeter(mi / 0.0000062137)
}

func MIToMM(mi Mile) Millimeter {
	return Millimeter(mi / 0.00000062137)
}

// Inches
func INToM(in Inch) Meter {
	return Meter(in / 39.370079)
}

func INToMI(in Inch) Mile {
	return Mile(in / 63360)
}

func INToFT(in Inch) Foot {
	return Foot(in * 0.083333)
}

func INToCM(in Inch) Centimeter {
	return Centimeter(in / 0.39370)
}

// Feet
func FTToIN(ft Foot) Inch {
	return Inch(ft * 12)
}

func FTToM(ft Foot) Meter {
	return Meter(ft / 3.2808)
}

func FTToMI(ft Foot) Mile {
	return Mile(ft * 0.00018939)
}
